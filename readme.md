# Particle-laden Viscoelastic Fluid Flow Simulation

## Installation guid for thi lib along side OpenFOAM and CFDEM Coupling

### Step 1

go to OpenFoam Wiki and follow instruction to install openFoam version 3.0.x
here (http://openfoamwiki.net/index.php/Installation/Linux/OpenFOAM-3.0.x/Ubuntu)

**NOTE:**
 if you follow the instruction for the third-party, you will see the paraview wont get installed. So advise download the theirdparty folder from version 3.0.1 and follow instruction there  (http://openfoamwiki.net/index.php/Installation/Linux/OpenFOAM-3.0.1/Ubuntu)

then comeback and install openfoam 3.0.x. For this thing to work with CFDEM vcersion that we install later , you need to checkout the release/commit number 
ac3f6c67e02f0aac3777c27f9fb755



### Step 2 

go to CFDEMCoupling website and follow instruction to install CFDEM and LIGGGTHS. Everything is pretty straightforward. Click here (https://www.cfdem.com/media/CFDEM/docu/CFDEMcoupling_Manual.html#installation)

**NOTE:**
We need CFDEM version 3.7.0 to work with OF 3.0.x
We need LIGGGH version 3.7.0 to work with above configuration

So when downloading the source code, just simply go to the github website and then go to “release” to find and download these working versions.

Now follow instruction to install CFDEM and LIGGGHT as explained in this link (https://www.cfdem.com/media/CFDEM/docu/CFDEMcoupling_Manual.html#installation)


### Step 3 

Copy the “downloadEigen” file from this repo and paste in in the ThirdParty-3.0.x folder, and follow the following instruction in your terminal

run  “chmod +x  downloadEigen”

run “./downloadEigen”

Then it gives you a line to be run in your terminal – copy-paste that commend and your are good to go.

### Step 4 

Copy the “extrapolatedCalculated” file from this repo and paste in a folder in the following path:
OpenFOAM/OpenFOAM-3.0.x/src/finiteVolume/fields/fvPatchFields/basic

then add the following line in the file:
$(basicFvPatchFields)/extrapolatedCalculated/extrapolatedCalculatedFvPatchFields.C

right below this line:
$(basicFvPatchFields)/basicSymmetry/basicSymmetryFvPatchScalarField.C

located in the “files” file in the make folder in this path:
OpenFOAM/OpenFOAM-3.0.x/src/finiteVolume/Make

Now open a terminal in this path, load of30x, and run “wmake libso” to install the new boundary condition you just copied.

### Step 5 

copy the “CFDEM_Collection” file from this repo and replace your own CFDEMcoupling-PUBLIC-3.0.x located in CFDEM folder with this (this files include the correct solvers for viscoelastic IBM)

Now follow the below instructions to make the new CFDEM solvers works:

1- in your terminal go to  CFDEMcoupling-PUBLIC-3.0.x file

2- write “cfde”, then press “tab” button. It has to get auto-completed to “cfdem” - if yes then continue.

3- run “wclean src”

4- run “wclean applications”

5- run “cfdemCompCFDEMsrc”

6- run “cfdemCompCFDEMsol”

7- run “cfdemCompCFDEMuti”





